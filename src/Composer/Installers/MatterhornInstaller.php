<?php
namespace Composer\Installers;

class MatterhornInstaller extends BaseInstaller
{
    protected $locations = array(
        'base'  => 'base',
        'contributed' => 'base/sites/all/modules/contributed',
        'custom' => 'base/sites/all/modules/custom',
        'site' => 'base/sites/{$name}',
    );
}
